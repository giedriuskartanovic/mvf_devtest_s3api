<?php

namespace App\api\Base;

use App\Lib\DigestAuth;

/**
 * Class RestrictedController
 * @package App\api\Base
 */
class RestrictedController extends Controller
{

    public function __construct()
    {
        $request = request();

        if (!app(DigestAuth::class)->verifyRequest($request)) {
            Response::send("error", [], "Unauthorized", 401)->send();
            exit;
        }
    }

}