<?php

namespace App\api\Base;

use Illuminate\Http\JsonResponse;

class Response
{

    /**
     * Unsuccessful response with 404 status
     * @param string $error
     * @return \Illuminate\Http\JsonResponse
     */
    public static function notFound($error = ''): JsonResponse
    {

        if (!$error) {
            $error = 'Not Found';
        }

        return static::send("error", [], $error, 404);
    }


    /**
     * Successful response with data
     * @param $data
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function data($data, $message = ''): JsonResponse
    {
        return static::send("success", $data, $message);
    }

    /**
     * @param string $status normally "success" or "error"
     * @param array $data
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public static function send($status, $data = [], $message = '', $statusCode = 200): JsonResponse
    {
        $output = ['status' => $status];

        if ($message) {
            $output['message'] = $message;
        }

        if ($data) {
            $output['data'] = $data;
        }

        return \Response::json($output, $statusCode);
    }

}