<?php

namespace App\api\v1;

use App\api\Base\Response;
use App\api\Base\RestrictedController;
use App\Lib\Customers\Accounts\AccountsRepository;
use App\Lib\Customers\CustomersRepository;

/**
 * Class AccountsController
 * @package App\api\v1
 */
class AccountsController extends RestrictedController
{

    /**
     * @var AccountsRepository
     */
    private $accounts;

    public function __construct(AccountsRepository $accounts)
    {
        parent::__construct();
        $this->accounts = $accounts;
    }

    public function show($guid, $field = null)
    {
        $account = $this->accounts->find($guid);

        if (!$account) {
            return Response::notFound('Account Not Found');
        }

        if (!$field) {
            $data = $account->toArray();
        } else {
            if (!isset($account->{$field})) {
                return Response::send('error', [], 'Field "' . $field . '"" does not exist');
            }
            $data = [$field => $account->{$field}];
        }

        return Response::data($data);
    }


}