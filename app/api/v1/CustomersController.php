<?php

namespace App\api\v1;

use App\api\Base\Response;
use App\api\Base\RestrictedController;
use App\Lib\Customers\Accounts\Account;
use App\Lib\Customers\CustomersRepository;
use App\Lib\DataAccessor;

/**
 * Class CustomersController
 * @package App\api\v1
 */
class CustomersController extends RestrictedController
{

    /**
     * @var CustomersRepository
     */
    private $customers;

    public function __construct(CustomersRepository $customers)
    {
        parent::__construct();
        $this->customers = $customers;
    }

    public function accounts($customerGuid)
    {

        $customer = $this->customers->find($customerGuid);

        if (!$customer) {
            return Response::notFound('Customer Not Found');
        }

        $data = [];

        $input = request()->input();

        if (!$input) {
            // No additional query parameters, just get all IDs
            $accountsIds = $customer->getAccountIds();
        } else {
            // Additional query parameters added
            // Retrieve all accounts and then work with them according to them
            $accounts = $customer->getAccounts();

            // Get a set of accounts with combined balance between <min> and <max>
            // This option cannot be combined with any others
            if (isset($input['total_combined_min']) && isset($input['total_combined_max'])) {

                $min = (float)$input['total_combined_min'];
                $max = (float)$input['total_combined_max'];

                //@TODO

            } else {

                if (!empty($input['lastname'])) {
                    // Filter by lastname
                    $accounts = $accounts->filter(
                        function (Account $a) use ($input) {
                            return $a->getLastname() == $input['lastname'];
                        }
                    );
                }

                if (!empty($input['firstname'])) {
                    // Filter by firstname
                    $accounts = $accounts->filter(
                        function (Account $a) use ($input) {
                            return $a->getFirstname() == $input['firstname'];
                        }
                    );
                }

                if (!empty($input['email_domain'])) {
                    // Filter by email domain
                    $accounts = $accounts->filter(
                        function (Account $a) use ($input) {
                            return $a->getEmailDomain() == $input['email_domain'];
                        }
                    );
                }

                if (!empty($input['negative_balance_only'])) {
                    // Filter only negative balance
                    $accounts = $accounts->filter(
                        function (Account $a) {
                            return $a->getBalance() < 0;
                        }
                    );
                }

                if (isset($input['min_balance'])) {
                    // Filter by minimum balance
                    $accounts = $accounts->filter(
                        function (Account $a) use ($input) {
                            return $a->getBalance() >= (float)$input['min_balance'];
                        }
                    );
                }


                if (isset($input['max_balance'])) {
                    // Filter by maximum balance
                    $accounts = $accounts->filter(
                        function (Account $a) use ($input) {
                            return $a->getBalance() <= (float)$input['max_balance'];
                        }
                    );
                }

            }

            // Sorting
            $allowedSorting = ['firstname', 'lastname', 'balance'];
            if (!empty($input['sort']) && in_array($input['sort'], $allowedSorting)) {

                // Sort type
                if (!empty($input['sort_type']) && in_array($input['sort_type'], ['asc', 'desc'])) {
                    $sortType = $input['sort_type'];
                } else {
                    $sortType = 'asc';
                }

                if ($input['sort'] == 'firstname') {
                    $accounts = $accounts->sort(
                        function (Account $a, Account $b) use ($sortType) {
                            return $sortType == 'asc' ?
                                ($a->getFirstname() > $b->getFirstname()) :
                                ($a->getFirstname() < $b->getFirstname());
                        }
                    );
                }elseif ($input['sort'] == 'lastname') {
                    $accounts = $accounts->sort(
                        function (Account $a, Account $b) use ($sortType) {
                            return $sortType == 'asc' ?
                                ($a->getLastname() > $b->getLastname()) :
                                ($a->getLastname() < $b->getLastname());
                        }
                    );
                }elseif ($input['sort'] == 'balance') {
                    $accounts = $accounts->sort(
                        function (Account $a, Account $b) use ($sortType) {
                            return $sortType == 'asc' ?
                                ($a->getBalance() > $b->getBalance()) :
                                ($a->getBalance() < $b->getBalance());
                        }
                    );
                }

            }

            $accountsIds = $accounts->pluck('guid')->all();
        }

        $data['accounts'] = $accountsIds;

        return Response::data($data);

    }

}