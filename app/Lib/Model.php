<?php

namespace App\Lib;

class Model
{

    protected $attributes = [];
    protected $hidden = [];

    public function toArray()
    {
        $attributes = $this->attributes;
        if (!empty($this->hidden)) {
            foreach ($this->hidden as $hidden) {
                unset($attributes[$hidden]);
            }
        }

        return $attributes;
    }

    public function __get($var)
    {
        if (isset($this->attributes[$var])) {
            return $this->attributes[$var];
        }

        return null;
    }

    public function __set($var, $value)
    {
        $this->attributes[$var] = $value;
    }

    public function __isset($var)
    {
        return property_exists($this, $var) || isset($this->attributes[$var]);
    }

}