<?php

namespace App\Lib\Customers;

use App\Lib\DataAccessor;
use Illuminate\Support\Collection;

/**
 * Class CustomersRepository
 * @package App\Lib\Customers
 */
class CustomersRepository
{

    /**
     * @var DataAccessor
     */
    private $data;

    function __construct(DataAccessor $dataAccessor)
    {
        $this->data = $dataAccessor;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getAll(): Collection
    {
        $customers = [];

        foreach ($this->data->getDisk()->allFiles('/') as $filename) {
            $path = pathinfo($filename);
            $guid = $path['filename'];

            // @ Could use $this->find() here but no reason to send a request for each one as we know for sure these files exist
            $customers[] = new Customer($guid);
        }

        return collect($customers);
    }

    /**
     * @param string $guid
     * @return Customer|null
     */
    public function find(string $guid)
    {

        $filepath = $guid . '.' . $this->data->getFileType();

        if ($this->data->getDisk()->exists($filepath)) {
            $customer = new Customer($guid);

            return $customer;
        }

        return null;

    }

}