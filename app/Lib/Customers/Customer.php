<?php

namespace App\Lib\Customers;

use App\Lib\Customers\Accounts\Account;
use App\Lib\Customers\Accounts\AccountsRepository;
use App\Lib\DataAccessor;
use App\Lib\Model;
use Illuminate\Support\Collection;

/**
 * Class Customer
 * @package App\Lib\Customers
 *
 * @property string $guid
 */
class Customer extends Model
{

    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return array
     */
    public function getAccountIds(): array
    {
        return $this->getAccounts()->pluck('guid')->all();
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return app(AccountsRepository::class)->getForCustomer($this->guid);
    }

}