<?php

namespace App\Lib\Customers\Accounts;

use App\Lib\Customers\CustomersRepository;
use App\Lib\DataAccessor;
use Illuminate\Support\Collection;

/**
 * Class AccountsRepository
 * @package App\Lib\Customers\Accounts
 */
class AccountsRepository
{

    /**
     * @var DataAccessor
     */
    private $data;

    /**
     * @var CustomersRepository
     */
    private $customers;

    function __construct(DataAccessor $dataAccessor, CustomersRepository $customers)
    {
        $this->data = $dataAccessor;
        $this->customers = $customers;
    }

    /**
     * @param string $guid
     * @return Collection
     */
    public function getForCustomer(string $guid): Collection
    {
        $customerData = $this->data->getDisk()->get($guid . '.' . $this->data->getFileType());
        $customerData = json_decode($customerData);

        $accounts = [];

        if ($customerData->accounts) {
            foreach ($customerData->accounts as $a) {
                $accounts[] = new Account(
                    $a->id, $a->firstname, $a->lastname, $a->email, $a->telephone, $a->balance
                );
            }
        }

        return collect($accounts);
    }

    /**
     * @param string $guid
     * @return Account|null
     */
    public function find(string $guid)
    {

        // Go through all accounts of all customers until the account is found
        foreach ($this->customers->getAll() as $customer) {
            foreach ($customer->getAccounts() as $account) {
                if ($account->getId() == $guid) {
                    return $account;
                }
            }
        }

        return null;

    }

}