<?php

namespace App\Lib\Customers\Accounts;

use App\Lib\Model;
use Illuminate\Support\Collection;

/**
 * Class Account
 * @package App\Lib\Customers\Accounts
 *
 * @property string guid
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property float balance
 */
class Account extends Model
{

    /**
     * Account constructor.
     * @param string $guid
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $telephone
     * @param float|string $balance
     */
    public function __construct(
        string $guid,
        string $firstname,
        string $lastname,
        string $email,
        string $telephone,
        $balance
    ) {
        $this->guid = $guid;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->telephone = $telephone;

        // If balance if not a numeric, get the numeric value by removing any non-numeric characters (like commas)
        if (!is_numeric($balance)) {
            $balance = floatval(preg_replace('/[^\d.-]/', '', $balance));
        }

        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->guid;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getEmailDomain()
    {
        return substr(strrchr($this->email, "@"), 1);
    }

}