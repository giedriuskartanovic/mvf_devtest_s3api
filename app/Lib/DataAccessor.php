<?php

namespace App\Lib;

use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Class DataAccessor
 * @package App\Lib
 */
class DataAccessor
{

    /**
     * @var FilesystemAdapter
     */
    private $disk;

    private $fileType = 'json';

    function __construct()
    {
        $this->disk = \Storage::disk('s3');
    }

    public function getDisk()
    {
        return $this->disk;
    }

    /**
     * Get the extension that data files are using. This could be overwritten for each Repository if different objects are using different file types.
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

}