<?php

namespace App\Lib;

use Illuminate\Http\Request;

class DigestAuth
{

    private $digestValidLength = (15 * 60); // 15 min

    /**
     * @param Request $request
     * @return bool
     */
    public function verifyRequest(Request $request): bool
    {

        $header = $request->header('Authentication');

        if (!$header) {
            // No auth info supplied
            return false;
        }

        $parts = explode(' ', $header);

        if (count($parts) !== 2) {
            // Invalid format
            return false;
        }

        $authType = $parts[0];
        if ($authType != 'custom-digest') {
            // Unsupported auth type
            return false;
        }

        list($timestamp, $digest) = explode(':', $parts[1]);
        if (!$timestamp || !$digest) {
            // Invalid formatting
            return false;
        }

        $diff = time() - $timestamp;
        if ($diff > $this->digestValidLength) {
            // Digest expired
            return false;
        }

        // Verify the digest
        if ($this->generateDigest($timestamp, $request) !== $digest) {
            // Invalid digest
            return false;
        }

        return true;
    }

    /**
     * @param int $timestamp
     * @param Request $request
     * @return string
     */
    public function generateDigest(int $timestamp, Request $request): string
    {
        $httpVerb = strtoupper($request->method());
        $uriPath = '/' . $request->path();
        $secretKey = \Route::current()->parameter('guid');

        return hash("sha256", $timestamp . $httpVerb . $uriPath . $secretKey);
    }

}